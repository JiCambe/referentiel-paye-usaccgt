Syntaxe
=======

Cette page est faite pour vous aider à écrire / mettre à jours les différentes page, que ce soit au format markdown (fichier .md) ou Restructured Test (fichier .rst)

TODO

Les liens internes
------------------

Pour faire un liens vers un endroit précis de la doc, nous allons utiliser le concept de cible/référence propre au logiciel.

Il y a deux types de cibles :
  * Celle défini automatiquement par le logiciel (cible implicite)
  * Celle défini par vous (cible explicite)

Les titres et sous titres sont automatiquement défini comme cible. Leur chemin est de la forme chemin/vers/dossier/nom_de_fichier_sans_extension:titre

Vous pouvez également définir des cibles par vous-même : 

.. tabs::

  .. group-tab:: Dans un fichier .rst 

    .. code-block:: rst

      .. _My target:

      Explicit targets
      ~~~~~~~~~~~~~~~~

  .. group-tab:: Dans un fichier .md

    .. code-block:: md

      (My_target)=
      ## Explicit targets

Ensuite, pour insérer un lien, nous allons utiliser la syntaxe suivante : 

.. tabs::

  .. group-tab:: Dans un fichier .rst 

    .. code-block:: rst

      - :ref:`essentiel/02-Par corps:IESSA`.
      - :ref:`Qui est-ce ? <essentiel/02-Par corps:IESSA>`.

  .. group-tab:: Dans un fichier .md

    .. code-block:: md

      - {ref}`essentiel/02-Par corps:IESSA`.
      - {ref}`Qui est-ce ? <essentiel/02-Par corps:IESSA>`.

Ce qui donne : 
  - :ref:`essentiel/02-Par corps:IESSA`.
  - :ref:`Qui est-ce ? <essentiel/02-Par corps:IESSA>`.

voir : https://docs.readthedocs.io/en/stable/guides/cross-referencing-with-sphinx.html