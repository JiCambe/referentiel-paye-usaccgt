# Fiche type d'un élément de rémunération

Fiche élément : 
  Nom: 
  
  Lignes correspondante fiche de paye : 
    - code:
	  libelle: (colonne élément)
	  ce que c'est:   Montant versé (= à payer) / Part salariale (= à déduire) / Part Patronale (=Pour information)

  Cas d'usage: (dans quel cas l'élément figurera sur la fiche de paye)

  Paramètres nécéssaire : (Un paramètre est une valeur qui ne dépend pas du cas de l'utilisateur. ex : valeur du point d'indice)

  Données nécéssaires : (valeur qui dépende du cas de l'utilisateur. Ce n'est pas forcément la question directe qui lui sera posé. ex : Indice Brut --> La question posé à l'utilisateur est son grade-échelon, l'IB est déduite de la réponse de l'utilisateur à cette question.)

  Calcul (cas général) : 

  Cas particulier :
    - cas 1 (résumé)
	  condition : (condition de déclenchement de ce cas particulier. ex : agent en guyanne)
	  calcul : 
	- cas 2 (résumé)
	  condition : (condition de déclenchement de ce cas particulier. ex : agent en guyanne)
	  calcul : 